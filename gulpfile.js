'use strict';

// Simplified gulpfile – run default task ('gulp') or watch task ('gulp watch') to take care of everything.

var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  combinemq = require('gulp-group-css-media-queries'),
  jshint = require('gulp-jshint'),
  cssnano = require('gulp-cssnano'),
  rename = require('gulp-rename'),
  sass = require('gulp-ruby-sass'),
  size = require('gulp-size'),
  uglify = require('gulp-uglify'),
  maps = require('gulp-sourcemaps'),
  del = require('del'),
  stripmq = require('gulp-stripmq'),
  inject = require('gulp-inject'),
  stylish = require('jshint-stylish');

var config = {
  sass: './scss/**/*.scss',
  css: './css',
  css_nomq: './css/nomq',
  js: './js',
  templates: './templates',
};

var autoprefixer_browsers = [
  '> 3%'
];

gulp.task('watch', ['compileSass','concatScripts'], function() {
//  gulp.watch(config.sass, ['styles-nomq']);
  gulp.watch(config.sass, ['compileSass','criticalSass','criticalCSS']);
  gulp.watch(config.js + '/*.js', ['concatScripts']);
  gulp.watch('./templates/*.*', ['criticalSass','criticalCSS']);
});

gulp.task('compileSass', function(){
  return sass(config.sass, {
    style: 'expanded',
    precision: 6,
    sourcemap: true
  })
  .pipe(maps.init())
  .pipe(rename('style.css'))
  .pipe(maps.write('./'))
  .pipe(gulp.dest(config.css))
});

// Uncomment to make a separate stylesheet for ie8

// gulp.task('styles-nomq', function() {
//   return sass(config.sass, {
//     style: 'expanded',
//     precision: 6
//   })
//     .pipe(stripmq({
//       options: {width: 1024},
//       type: 'screen'
//     }))
//     .pipe(autoprefixer({
//       browsers: autoprefixer_browsers,
//       cascade: false
//     }))
//     .pipe(gulp.dest(config.css_nomq))
//     .pipe(cssnano())
//     .pipe(rename({suffix: '.min'}))
//     .pipe(gulp.dest(config.css_nomq + '/minified'))
//     .pipe(size({title: 'css-nomq'}));
// });

gulp.task('minifySass', function(){
  gulp.src(config.css + '/style.css')
  .pipe(maps.init())
  .pipe(combinemq())
  .pipe(autoprefixer({
    browsers: autoprefixer_browsers,
    cascade: false
  }))
  .pipe(cssnano())
  .pipe(gulp.dest(config.css))
});

gulp.task('criticalSass', function() {
  return sass('./scss/critical.scss', {
    style: 'expanded',
    precision: 6,
    sourcemap: false
  })
  .pipe(combinemq())
  .pipe(autoprefixer({
    browsers: autoprefixer_browsers,
    cascade: false
  }))
  .pipe(cssnano())
  .pipe(gulp.dest(config.css))
});

// Matt S - Need to update the path for Template file to match the new drupal theme
gulp.task('criticalCSS', function() {
  gulp.src(['./templates/index.src.html'])
    .pipe(inject(gulp.src(['./css/critical.css']), {
      removeTags: 'true',
      starttag: '<!-- inject:criticalCSS:{{ext}} -->',
      transform: function (filePath, file) {
        // return file contents as string
        return file.contents.toString('utf8')
      }
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest(''));
});

gulp.task('lint', function() {
  gulp.src(config.js + '/*.js')
  .pipe(jshint())
  .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('concatScripts',['lint'], function(){
  return gulp.src([
    config.js + '/libs/feature.js',
    config.js + '/libs/owl.carousel.min.js',
    config.js + '/libs/tableit.js',
    config.js + '/libs/jquery.mediaWrapper.js',
    config.js + '/libs/jquery.doubletaptogo.js',
    config.js + '/*.js',
  ])
  .pipe(maps.init())
    .pipe(concat('production.js'))
    .pipe(maps.write('./'))
    .pipe(gulp.dest(config.js + '/build'))
});

gulp.task('minifyScripts', ['concatScripts'], function(){
  return gulp.src('js/build/production.js')
  .pipe(uglify())
  .pipe(gulp.dest(config.js + '/build'))
  .pipe(size({title: 'production js'}));
});

gulp.task('clean', function(){
  del([config.css, config.js + '/build', config.js + '/production']);
});

// ----------------------
// Default task
gulp.task('default', function(){
  gulp.start('watch');
});
