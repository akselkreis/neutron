# This is my README

##central.less
Compiles all other less files into a single file. This is where you would add new files, or remove/comment out old or unused files.

##general-styles.less
You would put your general styles here, unique type styles, block styles.

##header-footer.less
Use this file for your header and footer.

##Content Types
Within the content-types folder, there is a home and interior less file, ideally you would create more of these files to house the different styles for each content type. 
All media-queries for a content type would be housed on a per content-type basis.